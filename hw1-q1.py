import pandas as pd
from pandas import DataFrame
import plotly.express as px


# In[10]:


def process_data(file_name):
    # read and preprocess file
    df = pd.read_csv("{}.din".format(file_name), sep=" ", names=['operation', 'address'])
    df['decimal_address'] = df['address'].str.zfill(8).apply(lambda x: int(x, 16))

    # plot and save histogram
    fig = px.histogram(df, x="decimal_address", range_x=[df["decimal_address"].min(), df["decimal_address"].max()])
    fig.write_html("{}.html".format(file_name))
    
    # calculate operation frequency
    operation_counts = df['operation'].value_counts().reset_index()
    operation_counts.columns = ['operation', 'count']
    operation_counts['operation_name'] = operation_counts['operation'].map(
        {
            0: "read data",
            1: "write data",
            2: "instruction fetch"
        }
    )    
    operation_counts.to_excel("{}_operation_counts.xlsx".format(file_name))


# In[11]:


process_data('spice')


# In[12]:


process_data('tex')
