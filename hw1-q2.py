import numpy as np
import random
import time


intTimeA = 0

dblTimeA = 0

intTimeB = 0

dblTimeB = 0

num =10

i = 1

while i <=num:

 matrix1 = np.random.randint(4,100,size=(100,200))
 matrix2 = np.random.randint(5,200,size=(200,100))

 time_begin = time.time()

 R1 = np.dot(matrix1,matrix2)
 print("R1-----")
 print(R1)

 time_end = time.time()
 T = time_end - time_begin
 print('IntTime:%d'%(i),T)  
 intTimeA += T
 i +=1
 continue

i = 1

while i <= num:

 matrix3 = np.random.rand(200,300)      
 matrix4 = np.random.rand(300,200)

 time_begin = time.time()

 R2 = np.dot(matrix3,matrix4)
 print("R2-----")
 print(R2)
 
 time_end = time.time()
 T = time_end - time_begin
 print('Time:%d'%(i),T)                                    
 dblTimeA += T
 i +=1
 continue

intAvgTimeA = intTimeA / num

dblAvgTimeA = dblTimeA / num

print("intAvgTimeA",intAvgTimeA)
print("dblAvgTimeA",dblAvgTimeA)

i = 1

while i <=num:

 matrix5 = np.random.randint(4,100,size=(100,200))
 matrix6 = np.random.randint(5,200,size=(200,100))

 time_begin = time.time()

 R3 = matrix5@matrix6
 print("R3-----")
 print(R3)

 time_end = time.time()
 T = time_end - time_begin
 print('IntTime:%d'%(i),T)  
 intTimeB += T
 i +=1
 continue

i = 1

while i <= num:

 matrix7 = np.random.rand(200,300)      
 matrix8 = np.random.rand(300,200)

 time_begin = time.time()

 R4 = matrix7@matrix8
 print("R4-----")
 print(R4)
 
 time_end = time.time()
 T = time_end - time_begin
 print('Time:%d'%(i),T)                                    
 dblTimeB += T
 i +=1
 continue


intAvgTimeB = intTimeB / num

dblAvgTimeB = dblTimeB / num      


  

print("intAvgTimeB",intAvgTimeB)
print("dblAvgTimeB",dblAvgTimeB)




